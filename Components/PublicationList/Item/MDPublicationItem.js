import React from 'react';
import {View, Image, TouchableOpacity, Text , StyleSheet} from 'react-native';
import MDPubImage from './MDPubImage';
import { VirtualConsole } from 'jsdom';


/*
**Description: cet component est la représentation d'une publication vue par l'utilisateur en besoin de proprieté
**liste des props: propriete
*/

class MPublicationItem extends React.Component
{
    constructor(props)
    {
        super(props);

        this.imgExist=true;//modifier par rapport à l'existence de photos posté

        //cette attribut est le lien où sont situés les images
        this.imgGeneralLocation="http://10.0.0.2/Images/";

        //cette attribut est le lien où sont situés les images de profil
        this.imgProfileLocation="http://10.0.0.2/Images/Users";
        
        //emplacement des images de catégorie de l'application(maison ou terrain)
        this.imgCategorie;

        const {ville,quartier,categorie,type,nom,description} = this.props.propriete;

    }

    _ouvrirDetail()
    {
        
    }

    render(){
        return(
            <TouchableOpacity style={style.globalContainer} onPress={()=>this._ouvrirDetail()}>
                <View style={style.topContainer}>
                    <Image source={}/>
                    <Text>{nom}</Text>
                </View>
                <View style={style.middleContainer}>
                    <Image source={}/>
                    <Text>{description}</Text>
                </View>
                <View style={style.bottomContainer}>
                    <View style={style.bottomLeftContainer}>
                        <Text style={style.ville}>Ville: {Ville}</Text>
                        <Text style={style.quartier}>Quartier: {quartier}</Text>
                    </View>
                    <View style={style.bottomRightContainer}>
                        {this.imgExist? <MDPubImage style={style.imageCenter} imagesInfo={}/> : <Image style={style.imageCenter} source={{uri:this.imgCategorie}}/>}    
                    </View>
                </View>
                
            </TouchableOpacity>
        );
    }
}


const style = StyleSheet.create(
    {
        globalContainer:
        {
            height:100
        },
        topContainer:
        {
            flex: 1
        },
        middleContainer:
        {
            flex: 1
        },
        bottomContainer:
        {
            flex:4,
            flexDirection:"row"
        },

        bottomLeftContainer:
        {
            flex: 2,
            justifyContent: "space-around"
        },
        bottomRightContainer:
        {
            flex: 4
        },
        ville:
        {
            flex: 1
        },
        quartier:
        {
            flex: 1
        },
        icoUser:
        {
            width:50,
            height:50,
            borderRadius:25
        },
        icoCategorie:
        {
            width:60,
            height:60,
            borderRadius:30
        },
        imageCenter:
        {
            flex: 1
        }
    }
);

export default MPublicationItem;
